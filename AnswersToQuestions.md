1a) 
string_format = '"',string,'"';
string = {alphabetic_characters | digits | special_chars | '\"' | "\\" | ws};
alphabetic_characters = "A" | "a" | "B" | "b" | "C" | "c" | "D" | "d" | "E" | "e" | "F" | "f" | "G" | "g" | "H" | "h" | "I" | "i" | "J" | "j" | "K" | "k" | "L" | "l" | "M" | "m" | "N" | "n" | "O" | "o" | "P" | "p" | "Q" | "q" | "R" | "r" | "S" | "s" | "T" | "t" | "U" | "u" | "V" | "v" | "W" | "w" | "X" | "x" | "Y" | "y" | "Z" | "z";
digits = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";
special_chars = "@" | "!" | "#" | "$" | "%" | "^" | "/" | "." | "?" | " , "| "~" | "'" | '"' | "(" | ")" | "-" |"+" | "_" | "=" | "?" | "{" | "}" | "[" | "]" | "<" | ">" | "|" | "`";
ws = " ";

1b)
comment_format = ("/*", string, "*/") | ("//",string);
string = {alphabetic_characters | digits | special_chars | ws};
alphabetic_characters = "A" | "a" | "B" | "b" | "C" | "c" | "D" | "d" | "E" | "e" | "F" | "f" | "G" | "g" | "H" | "h" | "I" | "i" | "J" | "j" | "K" | "k" | "L" | "l" | "M" | "m" | "N" | "n" | "O" | "o" | "P" | "p" | "Q" | "q" | "R" | "r" | "S" | "s" | "T" | "t" | "U" | "u" | "V" | "v" | "W" | "w" | "X" | "x" | "Y" | "y" | "Z" | "z";
digits = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";
special_chars = "@" | "!" | "#" | "$" | "%" | "^" | "/" | "." | "?" | " , "| "~" | "'" | '"' | "(" | ")" | "-" |"+" | "_" | "=" | "?" | "{" | "}" | "[" | "]" | "<" | ">" | "|" | "`" | '"' | "\" ;
ws = " ";

1c)
email_format = (string1,"@", string1,".", string2) | (string1,"@", string1,".", string1, ".", string2)
string1 = {alphabetic_characters | digits};
string2 = {alphabetic_characters};
alphabetic_characters = "A" | "a" | "B" | "b" | "C" | "c" | "D" | "d" | "E" | "e" | "F" | "f" | "G" | "g" | "H" | "h" | "I" | "i" | "J" | "j" | "K" | "k" | "L" | "l" | "M" | "m" | "N" | "n" | "O" | "o" | "P" | "p" | "Q" | "q" | "R" | "r" | "S" | "s" | "T" | "t" | "U" | "u" | "V" | "v" | "W" | "w" | "X" | "x" | "Y" | "y" | "Z" | "z";
digits = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";

1d)
phone_number_format = digits,digits,digits,"-",n,digits,digits,"-",digits,digits,digits,digits;
digits = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";
n = digits - ("0" | "1");

1e)
binary_format = "0b",{binary_numbers};
binary_numbers = "0" | "1";

octal_format = "0o",{octal_numbers};
octal_numbers = "1" | "2" | "3" | "4" | "5" | "6" | "7";

decimal_format = (digits,"_"digits) | ("-",digits,"_"digits) | ("-",digits) | (digits,".",digits) | ("-",digits,".",digits);
digits = {"0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"};

hex_format = "0x",{hex_numbers | hex_letters};
hex_numbers = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";
hex_letters = "A" | "B" | "C" | "D" | "E" | "F";

decimalconstants_format = A,"e",N;
A = ({digits}) | ({digits},".",{digits});
N = {digits};
digits = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";

2-1) An example of a lexical error detected by the scanner would be a given set of characters that do not match a certain defined token pattern. For example, if a given string such as "3+4" was not given a definition within the lexer, it would result in a lexical error as the characters have no precise definition. 

2-2) An example of a syntax error which would be detected by the parser within Rust would be using an incorrect operator while writing a program. For example, at the end of a given function which is supposed to have a return type, the programmer accidentally types ";". 

2-3) Static semantic errors detected by a semantic analysis could be as simple as the use of incompatible types within Rust. For example, if a programmer assigned a i32 integer type to a u8 integer type. 

2-4) Ownership errors are pretty common in Rust as Rust has a unique concept of ownership, borrowing, and references. For example, in a program, by simply forgetting a "&" before a variable may cause an ownership error.

2-5) An example of a lifetime error caught by the lifetime analysis of variables would be creating a program in which a function is trying to use a reference to a variable that has already gone out of scope. This can usually be fixed by adding lifetime annotations.

3) In Rust, the limit on the value of usize is exactly 18446744073709551615. In the event of an arithmetic overflow, a Rust error appears saying "literal out of range for 'usize'" and the program is not able to compile. I believe that the implications of size limits on the portability is that given such large or standard limits to various integer types, programs can be easily used across different machines/compilers.

4) It is usually difficult to tell if a program is correct or not because sometimes, the program may compile and look like it is running smoothly, but there may be errors such as arithmetic or spelling errors within. If the program involves a bunch of numbers and calculations, it would be hard to tell if the program is correct until border cases are tested and the output is correct. To find bugs in my code, I usually start by planning out what I'm going to do using pseudocode with pencil and paper. After making the pseudocode into a running program and finding bugs, I retrace the source of the bug in order to try and fix it by adjusting the code. After testing, the types of bugs revealed in a program are usually syntax errors or variables that have gone out of scope. Bugs that can not be caught by testing include intent errors as the program will compile, but the program output differs from what is expected. 

5) See .png images

6-1) This grammar defines different types of balanced and nonbalanced trees as grammer is used to create parse trees with different recurring epsilon characters terminate recursion within the branches. If any node is a B, the result will be similar opening and closing signs "(" and ")". However, if the any node contains a N, the result will be different opening and closing signs "(" and "]". If the node is an L, it will contain only one left opening parenthese, "(". 

6-2 & 6-3) See .png images